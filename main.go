package main

import (
	"logging-record-api/web"
)

func main() {
	web.RunServer()
}
