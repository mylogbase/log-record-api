Application responsible for all logs-saving logic.

For now consist of two packages web and db.

web.server module has one main route to save logs
(/save/:clientToken/:parentTag/:childTag) // childTag - optional

this route triggers web.server.logSaving function which is main for now
consists of:
 - few validations
 - pulling data about customer (database ip and port, log-data quota left)
 - saving data to db if all ok
 - changing quota (reducing for request's data size)



#socket: too many open files issue:

solving:
ulimit -n
should be ~ 12000, not 1024 (too small)
so: ulimit -n 12000

SELECT SUM(LENGTH(log_body)) FROM ebbe05ca WHERE DATE(inserted_at) = CURRENT_DATE()

