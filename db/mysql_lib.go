package db


import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
	"os"
	"errors"
	"fmt"
	"encoding/json"
)

type idsForSync struct {
	LastId int `json:"last_id"`
	DbNum int  `json:"db_num"`
}

func getDb(dbUri string) (db *sql.DB, err error) {
	defer func() {
		if err := recover(); err != nil {
			err = errors.New("Error getting DB")
		}
	}()
	// debug config
	if (os.Getenv("ENV") == "local"){
		db, err = sql.Open("mysql", os.Getenv("DB_URI"))
	}else {
		db, err = sql.Open("mysql", dbUri)
	}
	return

}

// InsertLog main function to insert log to db
func InsertLog(dbUri, userToken, tag, childTag, logBody string, insertId chan string, dbNum int) (err error) {
	defer func() {
		if err := recover(); err != nil {
			err = errors.New("Error inserting row")
			insertId <- "null"
		}
	}()
	db, err := getDb(dbUri)

	if err != nil {
		log.Fatal(err.Error())
	}

	defer db.Close()

	stmtIns, err := db.Prepare("INSERT INTO "+ userToken +" (inserted_at, parent_tag, tag, log_body) VALUES(?, ?, ?, ?)")
	if r := recover(); r != nil{
		err = errors.New("Error preparing query")
	}
	defer stmtIns.Close()
	// TODO maybe define time before insertion goroutine?
	res, err := stmtIns.Exec(time.Now().UTC(), tag, NewNullString(childTag), logBody)

	lastId, err := res.LastInsertId()
	if r := recover(); r != nil{
		err = errors.New("Error inserting data")
	}

	insertId <- fmt.Sprintf(`{"last_id": %d, "db_num": %d}`, lastId, dbNum)
	return

}

func IdsForSyncSaving(userToken, data, db1, db2 string) (error) {
	defer func() {
		if err := recover(); err != nil {
			err = errors.New("Error saving sync ids")
		}
	}()
	tableName := fmt.Sprintf("%s_sync_ids", userToken)
	var db *sql.DB
	var err error
	var idsData idsForSync
	json.Unmarshal([]byte(data), &idsData)
	if idsData.DbNum == 1 {
		db, err = getDb(db1)
		// setting value to redis which db has issues to prevent reading from there until sync
		SetRedisVal("bad_db", "2", 1)
	}else if idsData.DbNum == 2 {
		db, err = getDb(db2)
		SetRedisVal("bad_db", "1", 1)
	}
	defer db.Close()
	if recover() != nil {
		log.Fatal(err.Error())
	}

	stmtIns, err := db.Prepare("INSERT INTO "+ tableName +" VALUES(?)")
	if r := recover(); r != nil{
		err = errors.New("Error preparing query")
	}
	defer stmtIns.Close()

	_, err = stmtIns.Exec(idsData.LastId)
	return err


}

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid: true,
	}
}
