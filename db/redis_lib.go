package db

import (
	"github.com/go-redis/redis"
	"encoding/json"
	"errors"
	"os"
)

func getRedisClient(dbNum int) *redis.Client{
	client := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Password: os.Getenv("REDIS_PASS"),
		DB:       dbNum,
	})

	return client
}

func SetRedisVal(key, value string, dbNum int) error {
	client := getRedisClient(dbNum)
	defer client.Close()
	return client.Set(key, value, 0).Err()
}

func GetRedisVal(key string, dbNum int) (string, error) {
	client := getRedisClient(dbNum)
	defer client.Close()
	return client.Get(key).Result()
}

func DelRedisVal(key string, dbNum int) error {
	client := getRedisClient(dbNum)
	defer client.Close()
	return client.Del(key).Err()
}

type redisStore struct {
	DbUri1 string  `json:"db_uri_1"` // primary db
	DbUri2 string  `json:"db_uri_2"` // secondary db
	DataLeft int `json:"data_left"`
}

// GetClientData returns structured client data which is stored as json in redis
// example: {"db_uri_1": "user:pass@(ip:port)/db?charset=utf8", "db_uri_2" : same, "data_left": 204800000}
func GetClientData(key string) (redisStore, error) {
	v, err := GetRedisVal(key, 0)
	var store = redisStore{}
	if err == redis.Nil {
		return store, errors.New("customer do not exists")
	}
	byt := []byte(v)
	json.Unmarshal(byt, &store)
	return store, err
}

func UpdateDataLeft(userToken string, oldStore redisStore, newVal int) error {
	oldStore.DataLeft = newVal
	data, _ := json.Marshal(oldStore)
	err := SetRedisVal(userToken, string(data), 0)
	return err
}