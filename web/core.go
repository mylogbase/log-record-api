package web

import (
	"fmt"
	"logging-record-api/db"
	"net/http"
	"github.com/julienschmidt/httprouter"
)


func logSaving(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// user token is table name to save logs
	userToken := ps.ByName("userToken")

	if len(userToken) <= 3 {
		http.Error(w, `{"error": "Wrong user token"}`, http.StatusBadRequest)
		return
	}

	parentTag := ps.ByName("parentTag")

	childTag := ps.ByName("childTag")

	data := r.PostFormValue("data")

	clientData, err := db.GetClientData(userToken)

	if err != nil {
		http.Error(w, `{"error": "Customer do not exists"}`, http.StatusBadRequest)
		return
	}

	dataLenth := len(data)


	if dataLenth > clientData.DataLeft {
		http.Error(w, `{"error": "Out of quota"}`, http.StatusConflict)
		return
	}

	// here logic to write in two databases in the same time asynchronously
	// chanel to save last inserted id number of db in which it was inserted
	// in case if another db not responding
	insertedId := make(chan string, 2)

	go db.InsertLog(clientData.DbUri1, userToken, parentTag, childTag, data, insertedId, 1)
	go db.InsertLog(clientData.DbUri2, userToken, parentTag, childTag, data, insertedId, 2)

	dataLeft := clientData.DataLeft - dataLenth

	err = db.UpdateDataLeft(userToken, clientData, dataLeft)

	res := <-insertedId
	res2 := <-insertedId

	if res == "null" && res2 != "null"{
		go db.IdsForSyncSaving(userToken, res2, clientData.DbUri1, clientData.DbUri2)
	}else if res != "null" && res2 == "null"{
		go db.IdsForSyncSaving(userToken, res, clientData.DbUri1, clientData.DbUri2)
	}else if res == "null" && res2 == "null"{
		http.Error(w, `{"error": "Error saving data"}`, http.StatusInternalServerError)
		return
	}

	if err != nil{
		http.Error(w, `{"error": "Error"}`, http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, `{"status": "ok", "quota_left": "%d"}`, dataLeft)

	r.Body.Close()
}
