package web

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"log"
)

// RunServer main function to run web server
func RunServer() {
	router := httprouter.New()
	router.POST("/save/:userToken/:parentTag", logSaving)
	router.POST("/save/:userToken/:parentTag/:childTag", logSaving)
	log.Fatal(http.ListenAndServe(":8080", router))
}
